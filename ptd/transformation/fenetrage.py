from ptd.model.transformation import Transformation
from ptd.model.tableau import Tableau
import time

class Fenetrage(Transformation):
    """
    Classe qui permet de sélectionner des données correspondant à une fenêtre de temps 

    Cette classe hérite de la classe Transformation
    
    Attributes
    ----------
    debut : date
        début de la fenêtre de temps
        
    fin : date 
        fin de la fenêtre de temps

    num_colonne : int
        numéro de la colonne où s'opère la sélection  
    """

    def __init__(self,debut,fin,num_colonne_date,sep_date='/'):
        """
        Constructeur

        Parameters
        ----------
        debut : str
            date de début de la plage sélectionnée
        fin : str
            date de fin de la plage sélectionnée
        num_colonne_date : int
            numéro de la colonne où se trouve la date
        sep_date='/' : str
            indique quel séparateur est utilisé dans le format de la date dasn le tableau
        """
        self.debut=debut
        self.fin=fin
        self.num_colonne_date=num_colonne_date
        self.sep_date=sep_date

    def transformation(self,donnees):
        """
        methode qui permet de sélectionner les données selon un critère temporel

        Parameters
        ----------
        donnees : Tableau
            données sur lesquels on veut appliquer la fenêtre de temps

        Returns
        -------
        Tableau
            données sélectionné selon la fenêtre de temps prédéfinnie

        Example
        -------
        >>> data=Tableau(['date','Temperature','jour_de_la_semaine','nbr_jour_pluie'],[['18/01/2022',20,'mardi',3],['19/01/2022',22,'mercredi',6],['20/02/2022',25,'jeudi',11]])
        >>> a1=Fenetrage('18/01/2022','19/01/2022',1)
        >>> print(a1.transformation(data))
        ['date', 'Temperature', 'jour_de_la_semaine', 'nbr_jour_pluie'],[['18/01/2022', 20, 'mardi', 3], ['19/01/2022', 22, 'mercredi', 6]]
        """
        res=[]
        for i in range(len(donnees.body)):
            if time.strptime(donnees.body[i][self.num_colonne_date-1], "%d/%m/%Y")>=time.strptime(self.debut, "%d"+self.sep_date+"%m"+self.sep_date+"%Y") and time.strptime(donnees.body[i][self.num_colonne_date-1], "%d/%m/%Y")<=time.strptime(self.fin, "%d"+self.sep_date+"%m"+self.sep_date+"%Y"):
                res.append(donnees.body[i]) #on fait self.num_colonne_date-1 car en python colonne commence à 0 mais l'utilisateur commence à compter à 1
        data=Tableau(donnees.header,res)
        return data

if __name__ == "__main__":
    import doctest
    doctest.testmod()
