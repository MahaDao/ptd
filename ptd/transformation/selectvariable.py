from ptd.model.transformation import Transformation
from ptd.model.tableau import Tableau

class selectvariable(Transformation):
    """Classe qui permet de sélectionner des variables d'un tableau

    Elle hérite de Transformation
    
    Attributes
    ----------
    liste_a_select : list
        données que l'on souhaite sélectionner 

    Examples
    --------
    >>> selec1 = selectvariable([])
    """
    def __init__(self):
        """
        Constructeur

        Parameters
        ----------
        list_a_select : list(str)
            liste des variables à sélectionner
        """
        pass

    def transformation(self,donnees,listevar):
        newheader=[]
        index_select=[]
        for i in range(len(donnees.header)):
            for j in range(len(listevar)):
                if self.listevar[j]==donnees.header[i]:
                    index_select.append(j)
                    newheader.append(donnees.header[i])
        premiere_ligne=[]
        for i in index_select:
            premiere_ligne.append(donnees.body[0][i])
        nouvelles_donnees=Tableau(newheader,premiere_ligne)
        for i in range(1,len(donnees.body)):
            body=[]
            for j in index_select:
                body.append(donnees.body[i][j])
            nouvelles_donnees.add_row(body)
        donnees=nouvelles_donnees
        return(donnees)

if __name__ == "__main__":
    import doctest
    doctest.testmod()