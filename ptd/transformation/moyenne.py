from ptd.model.transformation import Transformation
from ptd.model.tableau import Tableau
from selectvariable import Transformation

class Moyenne(Transformation):
    def __init__(self,donnees):
        self.donnees=donnees

    def transformation(self):
        """
        méthode qui permet de calculer la moyenne d'un Tableau

        au début la méthode sélectionne les index des variables quantitatives, 
        puis elle calcule le tableau des moyennes de ses variables en arrondissant les moyennes au centième près

        Parameters
        ----------
        donnees : Tableau
            Tableau sur lequel on veut calculer la moyenne

        Returns
        -------
        Tableau
            Tableau des moyennes des variables quantitatives

        Examples
        -------
        >>> data=Tableau(['Temperature','jour_de_la_semaine','nbr_jour_pluie'],[[20,'mardi',3],[22,'mercredi',6],[25,'jeudi',11]])
        >>> a1=Moyenne()
        >>> print(a1.transformation(data))
        ['Temperature', 'nbr_jour_pluie'],[22.33, 6.67]
        """
        moyenne=[]
        nbr_ligne=self.donnees.nb_obs()
        index_var_quanti=[]
        new_header=[]               #liste qui ne garde que les nom de colonnes de variables quantitatives
        for i in range(nbr_ligne):
            if isinstance(self.donnees.body[0][i], (int, float)):    #on vérifie que body[i][1] est un chiffre
                index_var_quanti.append(i)
        for i in index_var_quanti:                  
            data_col_i=[]                              #dans cette liste on regroupe toutes les données de la colonne i pour en calculer la moyenne
            new_header.append(self.donnees.header[i])
            for j in range (nbr_ligne):
                data_col_i.append(self.donnees.body[j][i])
            moyenne.append(round(sum(data_col_i)/len(data_col_i),2))
        tableau_moyenne=Tableau(new_header,moyenne)
        return tableau_moyenne

if __name__ == "__main__":
    import doctest
    doctest.testmod()