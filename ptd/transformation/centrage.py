from ptd.model.transformation import Transformation
from ptd.model.tableau import Tableau
from ptd.transformation.moyenne import Moyenne


class Centrage(Transformation):
    """
    classe qui permet de centrer un jeux de données
    """
    def __init__(self):
        pass

    def transformation(donnees: Tableau ,variable):
        """
        Il prend un tableau et une variable, trouve l'indice de la variable dans le tableau, puis trouve
        la moyenne des valeurs dans la colonne du tableau correspondant à la variable, puis soustrait la
        moyenne de chaque valeur dans la colonne, et enfin retourne un nouveau tableau avec les
        nouvelles valeurs centrées.
        
        :param tableau: Tableau
        :type tableau: Tableau
        :param variable: la variable à centrer
        :return: un objet Tableau.
        
        Returns
        -------

        Examples
        --------
        >>> data=Tableau(['Temperature','jour_de_la_semaine','nbr_jour_pluie'],[[25,'lundi',3],[22,'Mardi',6],[25,'Mercredi',11]])
        >>> a2=Centrage()
        >>> a3=Centrage.transformation(data,'Temperature')
        >>> print(a3)
        ['Temperature', 'jour_de_la_semaine', 'nbr_jour_pluie'],[[1.0, 'lundi', 3], [-2.0, 'Mardi', 6], [1.0, 'Mercredi', 11]]
        """
        index=(donnees.header).index(variable)
        data_col_i=[]                             
        for j in range (len(donnees.body)):
                data_col_i.append(donnees.body[j][index])
        moyenne=(sum(data_col_i)/len(data_col_i))
        for i in range(len(donnees.body)):
            donnees.body[i][index]=(donnees.body[i][index]-moyenne)
        
        return Tableau(donnees.header, donnees.body)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
