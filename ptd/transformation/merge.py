from ptd.model.tableau import Tableau
from ptd.model.transformation import Transformation

class Merge(Transformation):
    """
    Classe qui permet de fusionner deux jeu de données 
    
    Attributes
    ----------
    SecondaryTable: Tableau
        deuxième tableau avec leuqel on veut faire la jointure        
    PrimaryKey : str
        première clé de fusion
    SecondaryKey : str
        seconde clé de fusion

    Methods
    -------
    transformation()
        Cette méthode permet de fusionner deux tableaux et de retourner le tableau en résultant
    """
    def __init__(self, SecondaryTable : Tableau, PrimaryKey : str, SecondaryKey : str = None):
        self.SecondaryTable = SecondaryTable
        self.PrimaryKey = PrimaryKey
        self.SecondaryKey = SecondaryKey
    
    def transformation(self, table : Tableau):    
        """
        Cette méthode opère une jointure entre deux tables
        
        Elle prend une table et une table secondaire et retourne une nouvelle table qui est le résultat
        d'une jointure entre les deux tables
        
        Parameters
        ----------
        table : Tableau
            table à merge

        Returns
        -------
        Jointure : tableau
        """
        if self.SecondaryKey==None :
            self.SecondaryKey = self.PrimaryKey  
      
        SecondarykeyColummnPosition = self.SecondaryTable.header.index(self.SecondaryKey)
          
        PrymaryKey_indexes = table.cle_et_indices(column_index= table.header.index(self.PrimaryKey) )
        SecondaryKey_indexes = self.SecondaryTable.cle_et_indices( column_index= SecondarykeyColummnPosition )

        observations = []
        for cle in PrymaryKey_indexes :
            for k in PrymaryKey_indexes[cle] :
                Gauche = table.body[k]
                if cle in SecondaryKey_indexes :
                    for l in SecondaryKey_indexes[cle]:
                        Droite = self.SecondaryTable.body[l]
                        ligne = list(Gauche)
                        ligne.extend(Droite)
                        observations.append(ligne)
                else :
                    Droite = [None] * self.SecondaryTable.nb_col()
                    ligne = list(Gauche)
                    ligne.extend(Droite)
                    observations.append(ligne)
        
        names = list(table.header)
        names.extend(self.SecondaryTable.header)
        Jointure = Tableau(header=names,body=observations)
        Jointure.delete_col(position = table.nb_col() + SecondarykeyColummnPosition )
        return Jointure

class Leftjoin(Merge):
    
    def __init__(self, SecondaryTable: Tableau, PrimaryKey: str, SecondaryKey: str = None):
        """
        La fonction prend une table et renvoie la table avec la table secondaire qui lui est jointe.
         
        
        :param SecondaryTable: La table que vous souhaitez joindre à la table principale
        :type SecondaryTable: Tableau
        :param PrimaryKey: Colonne de la table principale qui sera utilisée pour joindre les deux tables
        :type PrimaryKey: str
        :param SecondaryKey: Le nom de la colonne dans la table secondaire qui sera utilisée pour
        joindre les deux tables
        :type SecondaryKey: str
        Examples:
        a1=Tableau(['lieu','température'],[[1,2],[2,3]])
        a2=Tableau(['lieu','population'],[[1,120],[2,250]])
        a3=Leftjoin(a1,'lieu')
        a4=a3.transformation(a2)
        print(a4)
        """
        super().__init__(SecondaryTable, PrimaryKey, SecondaryKey)
    
    def transformation(self, table: Tableau):
        return super().transformation(table)


class Rightjoin(Merge):
    
    def __init__(self, SecondaryTable: Tableau, PrimaryKey: str, SecondaryKey: str = None):
        super().__init__(SecondaryTable, PrimaryKey, SecondaryKey)
    
    def transformation(self, table: Tableau):
        return super().transformation(table)



