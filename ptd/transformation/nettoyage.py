from ptd.model.tableau import Tableau
from ptd.model.transformation import Transformation

class Nettoyage(Transformation):
    """Class qui permait de remplacer toutes les valeurs manquantes numérique
    par la moyenne de la colonne 
    """
    def __init__(self):
        pass

    def transformation(self,donnees= Tableau, marquage_val_mq=[]):
        """méthode qui permet de nettoyer les jeux de données
        
        elle remplace les valeurs manquantes numérique par la moyenne de la colonne,
        la méthode va demander comment sont noté les valeurs manquantes dans le jeux de données,
        cette att


        Parameters
        ----------
        donnees : tableau
            jeu de données sous forme de tableau
        marquage_val_mq=[] : list(str)
            comment sont marqué les valeurs manquantes dans les données fournit, c'est une liste 
            car dans un même jeu de données il peut y avoir plusieurs façons de noter des valeurs manquantes 

        Examples
        ---------
        >>> data=Tableau(['Temperature','jour_de_la_semaine','nbr_jour_pluie'],[[20,'mardi',3],[22,'NA',6],['NA','jeudi',11]])
        >>> a1=Nettoyage()
        >>> print(a1.transformation(data))
        ['Temperature', 'jour_de_la_semaine', 'nbr_jour_pluie'],[[20, 'mardi', 3], [22, 'NA', 6], [21.0, 'jeudi', 11]]
        """
        nbr_ligne=donnees.nb_obs()
        nbr_col=donnees.nb_col()
        index_var_quanti=[]
        list_bis=[]
        for i in range(len(marquage_val_mq)):               #on convertit tous les éléments de la liste en str
            list_bis.append(str(marquage_val_mq[i]))
        list_val_mq=['NONE','MQ','VALEUR MANQUANTE','NAN','NNN','NA','VM','VMQ','N/A','--','-']+list_bis   
        for i in range(nbr_col):
            if isinstance(donnees.body[0][i], (int, float)):    #on vérifie que body[i][1] est un chiffre
                index_var_quanti.append(i)
        for i in index_var_quanti:
            data_col_i=[]
            for j in range (nbr_ligne):
                if isinstance(donnees.body[j][i], (int, float)):
                    data_col_i.append(donnees.body[j][i])
            for j in range(nbr_ligne):
                if type(donnees.body[j][i])==str:
                    if donnees.body[j][i].upper() in list_val_mq:  #on utilise la liste de marquage type des valeurs manquantes
                        donnees.body[j][i]=sum(data_col_i)/len(data_col_i)
        return(donnees)

if __name__ == "__main__":
    import doctest
    doctest.testmod()