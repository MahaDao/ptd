

from ptd.model.tableau import Tableau


class Agregation_spatiale(Transformations):

    def __init__(self, variable="National"):
        '''

        Attributes
        ----------
        variable : str
            variable sur laquelle on fait l'agrégation, initalisée à National
        '''
      
        self.variable=variable

from manipulation import Manipulation
from donnees import Donnees
from moyenne import Moyenne
from somme import Somme
import numpy as np

class Agregation(Manipulation):

      def applique(self,donnees):
            #Récupération des indices des variables
            indice_agrege = donnees.variables.index(self.nom_var_agrege)
            #Opération préliminaire : comptage du nombre de valeurs distincts de la variable qui va être agrge ainsi que des valeurs
            count = 0  
            l=[]
            for k in range(len(donnees.donnees)):
                    if donnees.donnees[k][indice_agrege] not in l :  
                        count += 1
                        l.append(donnees.donnees[k][indice_agrege])
            #Opération préliminaire : Création de la table donnees_finales
            donnees_finales = [[0 for k in range(len(self.indication_type_var)) ] for n in range(count)]
            #Agregation des variables
            #triage des données par valeur distincte
            for k in range(len(self.indication_type_var)):
                    for n in range(count):
                        ligne=[]
                        for x in range(len(donnees.donnees)):
                                if donnees.donnees[x][indice_agrege]==l[n]:
                                    ligne.append(donnees.donnees[x])
                        #En fonction du type de la variable, différente agrégation
                        if self.indication_type_var[k][1]=="quant_int":
                              donnees_finales[n][k]=Moyenne.moyenne(Donnees(donnees.variables,ligne),self.indication_type_var[k][0],self.indication_type_var[k][2])
                        if self.indication_type_var[k][1]=="quant_ext":
                              donnees_finales[n][k]=Somme.somme(Donnees(donnees.variables,ligne),self.indication_type_var[k][0])
            #On ajoute la variable d'agrégation à la fin
            for indice in range(len(donnees_finales)):
                  donnees_finales[indice].insert(0,l[indice])
            nouvelle_donnee=Donnees([self.nom_var_agrege]+[self.indication_type_var[k][0] for k in range(len(self.indication_type_var))],donnees_finales)
            return nouvelle_donnee
            

      variables=["poids","euro","temperature"]
      donnees=[[500,10,200],[500,40,400],[1000,10,200],[1000,40,400]]
      test=Donnees(variables,donnees)
      agregation=Agregation("poids",[["euro","quant_ext"],["temperature","quant_int",None]])
      #l'agregation se fait sur le poids et on a bien définit euro comme variable quantitative extensive et temperature comme variable quantitative intensive et sans variable de pondération
      test_agregation=agregation.applique(test)
      res=[[500,50,300],[1000,50,300]]


