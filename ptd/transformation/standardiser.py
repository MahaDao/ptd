from ptd.model.transformation import Transformation
from ptd.model.tableau import Tableau


class Standardiser(Transformation):
    def __init__(self):
        pass
    
    def transformation(self,donnees : Tableau):
        """méthode qui permet de standardiser un tableau

        la méthode vérifie d'abord quelles sont les colonnes en float ou en int 
        puis standardise ses colonnes, la méthode arrondie la valeur a deux décimales

        Parameters
        ----------
        donnees : Tableau
            tableau de données que l'on veut standardiser

        Returns
        -------
        Tableau
            tableau de données standardisé

        Examples
        -------
        >>> data=Tableau(['Temperature','jour_de_la_semaine','nbr_jour_pluie'],[[20,'mardi',3],[22,'mercredi',6],[25,'jeudi',11]])
        >>> a1=Standardiser()
        >>> print(a1.transformation(data))
        ['Temperature', 'jour_de_la_semaine', 'nbr_jour_pluie'],[[-0.55, 'mardi', -0.34], [-0.08, 'mercredi', -0.06], [0.63, 'jeudi', 0.4]]
        """
        
        
        nb_ligne=donnees.nb_obs()
        nb_col=donnees.nb_col()
        moyenne=[0 for i in range(nb_col)]
        ecart_type=[0 for i in range(nb_col)]
        index_var_quanti=[]
        for i in range(nb_col):
            if isinstance(donnees.body[0][i], (int, float)):  
                index_var_quanti.append(i)
        for i in index_var_quanti:                  
            data_col_i=[]                             
            for j in range (nb_ligne):
                data_col_i.append(donnees.body[j][i])
            moyenne[i]=(sum(data_col_i)/len(data_col_i))
            ecart_type[i]=(sum((l-moyenne[i])**2 for l in data_col_i) / len(data_col_i))
        for i in index_var_quanti:
            for j in range(nb_ligne):
                donnees.body[j][i]=round((donnees.body[j][i]-moyenne[i])/ecart_type[i],2)
        return donnees
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
                
