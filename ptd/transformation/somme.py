from ptd.model.transformation import Transformation
from ptd.model.tableau import Tableau

class Somme(Transformation):
    '''Permet de calculer la somme d'une variable numérique

    Attributs
    ---------
    num_variable : list(int)
        numéro de la colonne dont on veut calculer la moyenne
    '''

    def __init__(self, num_variable):
        self.num_variable=num_variable

    def transformation(self,donnees):
        """        
        la méthode prend une liste de nombres et renvoie la somme des nombres de la liste et
        un message d'avertissament est affiché si il y a des valeurs manquantes

        Returns
        -------
        Tableau
            tableau des variables sur lesquelle on a fait la somme

        Examples
        -------
        >>> data=Tableau(['date','Temperature','jour_de_la_semaine','nbr_jour_pluie'],[['18/01/2022',20,'mardi',3],['19/01/2022',22,'mercredi',6],['20/02/2022',25,'jeudi',11]])
        >>> a1=Somme([2,4])
        >>> print(a1.transformation(data))
        ['Temperature', 'nbr_jour_pluie'],[67.0, 20.0]
        """
        new_header=[]
        list_somme=[]
        for i in self.num_variable:
            S=0
            new_header.append(donnees.header[i-1])
            for j in range(len(donnees.body)):
                S+=float(donnees.body[j][i-1])  #on fait i-1 car en python colonnes commence à 0 alors que l'utilisateur commence à compter à 1
            list_somme.append(S)
        tableau_somme=Tableau(new_header,list_somme)
        return tableau_somme

if __name__ == "__main__":
    import doctest
    doctest.testmod()