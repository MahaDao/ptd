from ptd.model.tableau import Tableau
from ptd.model.transformation import Transformation
from ptd.transformation.moyenne import Moyenne

class Moyenne_glissante(Transformation):
    '''
    Classe des transformations par moyenne glissante

    La moyenne glissante modifie une table contenant une seule variable,
    et renvoie une table contenant dans chaque case la moyenne des "periodes"
    valeurs suivantes. On appelle "periode" le pas de la moyenne glissante.

    Attributes
    ----------
    periode : int
        Pas de la moyenne glissante
    '''
    
    def __init__(self):
        '''

        '''

    def transformation(periode: int, donnees: Tableau, var):
        """
        Il prend une table, un nom de variable et une période et renvoie une liste des moyennes glissantes
        de la variable sur la période
        
        :param periode: le nombre de jours sur lesquels vous voulez faire la moyenne
        :type periode: int
        :param donnees: Tableau
        :type donnees: Tableau
        :param var: la variable dont vous voulez calculer la moyenne glissante
        :return: une liste de la moyenne des valeurs de la variable dans le tableau.
        
      
        Examples
        --------
        >>> data=Tableau(['Temperature','jour_de_la_semaine','nbr_jour_pluie'],[[25,'lundi',3],[22,'Mardi',6],[25,'Mercredi',11],[27,'lundi',3],[30,'lundi',3]])
        >>> print(Moyenne_glissante.transformation(2, data,'Temperature'))
        [23.5, 23.5, 26.0, 28.5]
        """
        index=(donnees.header).index(var)                       
        L=[]
        data_col_index=[]                             
        for j in range (len(donnees.body)):
                data_col_index.append(donnees.body[j][index])
        le=len(data_col_index)
        for i in range (le-periode+1):
            l=[]
            for j in range (periode):
                l.append(data_col_index[i+j])
            moyenne=(sum(l)/len(l))
            L.append(moyenne)
        return L

if __name__ == "__main__":
    import doctest
    doctest.testmod()