import gzip
import csv
import json
from ptd.model.tableau import Tableau
from ptd.model.transformation import Transformation

class Importation(Transformation):
    """
    Classe qui permet de charger un jeu de données 

    Cette classe permet de convertir les fichier de type
    json ou csv en type tableau
    
    Attributes
    ----------
    typefichier: str
        Correspond au type de fichier dans lequel il y a les données
    dossier : str
        Correspond au dossier ou se trouve le fichier
    fichier : str
        Correspond ou nom du fichier
    
    Methods
    -------
    transformation()
        Cette méthode permet de retourner un tableau, composé du jeu de donné du fichier.
    """

    def __init__(self, typefichier, dossier, fichier):
        """ 
        Constructeur

        Parameters
        ----------
        typefichier: str
            Correspond on type de fichier sur lequel il y a le jeu de donné
        dossier : str
            Correspond au dossier ou se trouve le fichier
        fichier : str
           Correspond ou nom du fichier
        """
        self.typefichier = typefichier
        self.dossier = dossier
        self.fichier = fichier
        self.donnees = []

    def transformation(self):
        """
        Returns
        -------
        donnees: Tableau()
            Tableau composé du jeu de donné du fichier. 
        >>> a1= Importation("csv","C:/Users/Cédric/OneDrive/Documents/ENSAI/ptd/donnees/données_météo","synop.201301")
        >>> a1.transformation()
        >>> a2= Importation("json","C:/Users/Cédric/OneDrive/Documents/ENSAI/ptd/donnees/données_électricité","2013-01")
        >>> print(a2.transformation())
        """
        if self.typefichier == "csv":
            # Dossier où se trouve le fichier :
            data = []
            with gzip.open(self.dossier + self.fichier, mode='rt') as gzfile :
                synopreader = csv.reader(gzfile, delimiter=' ;')
                for row in synopreader :
                    data.append(row)
            self.donnees=data
            
            
            
            
            
            # donnees = []
            # with gzip.open(self.dossier+"/"+self.fichier+"."+self.typefichier+".gz", mode='rt') as gzfile :
            #     synopreader = csv.reader(gzfile, delimiter=';')
            #     for row in synopreader :
            #         donnees.append(row) 
            # self.data = Tableau(donnees[0], donnees[1:len(donnees)])
            # return self.data
        
        elif self.typefichier == "json":
            with gzip.open(self.dossier+"/"+self.fichier+"."+self.typefichier+".gz", mode='rt') as gzfile :
                donnees = json.load(gzfile)

            header = list(donnees[0]['fields'].keys())
            n = len(donnees)
            empty= [['MQ' for i in range(len(header))]for i in range(n) ]
            get_items = donnees[0]['fields'].items()

            for i in range(n):
                ligne_courante = list(donnees[i]['fields'].items())
                for j in range(min(len(header),len(ligne_courante))):
                    z = ligne_courante[j]
                    cpt = 0
                    for key in header:
                        if key == z[0]:
                            empty[i][cpt] = z[1]
                            break
                        else:
                            cpt += 1
            self.data = Tableau(header, empty)
            return self.data
            
        else : 
            raise ValueError("Type de fichier non pris en charge")
        
a1= Importation("csv","C:/Users/Cédric/OneDrive/Documents/ENSAI/ptd/donnees/données_météo/","synop.201301")
meteo=a1.transformation()
a2= Importation("json","C:/Users/Cédric/OneDrive/Documents/ENSAI/ptd/donnees/données_électricité/","2013-01")
elec=a2.transformation()
print(a2.transformation()) 




#if __name__ == "__main__":
#    import doctest
#    doctest.testmod()