from ptd.model.transformation import Transformation

class Pipeline(Transformation):
    """
    class qui permet le fonctionnement du pipeline
    
    Methods
    -------
    transformation()
        Cette méthode permet de lancer le pipeline
    """
    def __init__(self,list_transfo):
        """
        Constructeur

        Parameters
        ----------
        list_transfo : list(transformation)
            Ensemble des transformations que l'on veut appliquer aux données
        """
        self.list_transfo=list_transfo

    def lancer(self,donnees):
        """
        méthode qui permet d'appliquer 
        le pipeline auxdonnées

        Parameters
        ----------
        donnees : Tableau
            Tableau de données que l'on veut modifier

        Examples
        -------
        >>> donnees=Tableau(['température_moyenne','nbr_jour_pluie'][20,57][22,44][16,92])
        >>> Pipel=Pipeline([nettoyage,moyenne])
        >>> pipeline.lancer(donnees)
        [température_moyenne,nbr_jour_pluie][19.3,64.3])
        """
        for i in self.list_tranfo:
            res=i.transformation(donnees)
            donnees=res