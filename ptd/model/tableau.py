from ctypes import FormatError


class Tableau:
    ''' 
    classe  permettant la création de tableau sous la forme d'une liste de liste 
    
    Parameters
    -------------
    header : list
        Première ligne qui contient les noms des variables
    body : list[list]
        Corps du tableau 
    
    Examples
    ---------
    >>> a=Tableau(['nom','prénom'],[['mallick', 'gael'], ['dao', 'maha'], ['grandvaux', 'cedric']])
    >>> a.nb_obs()
    3
    >>> a.nb_col()
    2
    >>> print(a.header)
    ['nom', 'prénom']
    >>> a.add_row(['hans','peter'])
    >>> print(a.body)
    [['mallick', 'gael'], ['dao', 'maha'], ['grandvaux', 'cedric'], ['hans', 'peter']]
    >>> a.delete_row(2)
    >>> print(a.body)
    [['mallick', 'gael'], ['dao', 'maha'], ['hans', 'peter']]
    >>> a.add_col(['age',1,2,3])
    >>> print(a.header, a.body)
    ['nom', 'prénom', 'age'] [['mallick', 'gael', 1], ['dao', 'maha', 2], ['hans', 'peter', 3]]
    >>> a.delete_col(2)
    >>> print(a.header, a.body)
    ['nom', 'prénom'] [['mallick', 'gael'], ['dao', 'maha'], ['hans', 'peter']]
    >>> a.cle_et_indices(0)
    {'mallick': [0], 'dao': [1], 'hans': [2]}
    '''
    def __init__(self, header : list, body : list):
        self.header=header
        self.body=body

    def nb_obs(self):
        """
        Renvoie le nombre d'observations dans la base de données
        :return: La longueur du corps.
        """
        return len(self.body)

    def nb_col(self):
        """
        Il renvoie le nombre de colonnes dans la table
        :return: Le nombre de colonnes dans l'en-tête.
        """
        return len(self.header)
    
    def add_row(self,row : list):    
        """
        Il prend une ligne de données et l'ajoute au corps de la table
        
        :param row: Une liste de valeurs à ajouter au tableau
        """
        self.body.append(row)
    
    def delete_row(self, row : int ):
        """
        Il supprime la ligne à l'index i du corps de la table
        
        :param i: le numéro de ligne à supprimer
        """
        del (self.body[row])
    
    def add_col(self,col : int):
        """
        Il prend une liste de valeurs et l'ajoute à la fin de la table en tant que nouvelle colonne
        
        :param col: une liste de valeurs
        """
        
        self.header.append(col[0])
        for i in range(len(self.body)):
            self.body[i].append(col[i+1])
    
    def delete_col(self,position:-1):
        """
        Supprime la colonne à la position spécifiée par l'utilisateur
        
        :param position: la position de la colonne à supprimer
        :type position: int
        :return: Rien
        """
        n = len(self.body)
        if position == -1 :
            position = n - 1
        for k in range(n):
            self.body[k].pop(position)
        self.header.pop(position)
        return

    def __str__(self):
        n = self.nb_obs()
        str=""
        if n<=10:
            str=self.body
        else:
            str=[self.body[0:5],self.body[-5:n]]
        return"{},{}".format(self.header,str)

    def cle_et_indices(self, column_index : int):
        """
        Il prend un index de colonne et renvoie un dictionnaire dont les clés sont les valeurs uniques
        de cette colonne et dont les valeurs sont des listes d'index des lignes dans lesquelles ces
        valeurs apparaissent
        
        :param column_index: l'index de la colonne à utiliser pour le dictionnaire
        :type column_index: int
        :return: Un dictionnaire avec les valeurs uniques de la colonne en tant que clés et les indices
        des lignes où la valeur se trouve en tant que valeurs.
        """
        dict = {}
        for l in range(self.nb_obs()) :
            if self.body[l][column_index] not in dict :
                d_up = { self.body[l][column_index] : [l] }
                dict.update( d_up )
            else :
                dict[ self.body[l][column_index] ].append(l)
        return dict

if __name__ == "__main__":
    import doctest
    doctest.testmod()