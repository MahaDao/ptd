from abc import ABC, abstractmethod

class Transformation(ABC):
    """
    class qui mets en place la méthode transformation qui 
    sera utilisé dans toutes les classes filles
    """
    def __init__(self):
        pass
        
    @abstractmethod
    def transformation(self,donnees):
        """
        méthode abstraite qui permet d'appeler la méthode dans les classes filles
        """
        pass