from ptd.model.transformation import Transformation
from ptd.model.tableau import Tableau
import csv

class Exportation(Transformation):
    """
    Classe qui permet l'exportation des données traitées

    La classe exportation dérive de la classe abstraite transformation contenue dans le package model
    
    Attributes
    ----------
    donnees : tableau
        données que l'on souhaite exporter 

    type_sortie : str
        texte qui défini le type de sortie voulu
    """

    def __init__(self, type_sortie,dossier,nom_fichier_a_ecrire):
        self.type_sortie=type_sortie
        self.dossier = dossier
        self.nom_fichier_a_ecrire = nom_fichier_a_ecrire        
    
    def transformation(self,donnees):
        """
        Permet d'exporter les données traitées en format CSV ou Map
        
        la méthode lève indique une erreur si le format demandé est inconnue, 
        et on peut exporter le nombre de lignes et de colonnes que l'on souhaite, 
        par défaut c'est tout le tableaude données

        Parameters
        ----------
        donnees : tableau
            données que l'on souhaite exporter
        nb_lignes : int
            indique le nombre de ligne à afficher
        nb_colonnes : int
            indique le nombre de colonne à afficher
        Returns
        -------
        CSV ou Map
            renvoie les données traitées sous forme CSV ou Map

        Examples
        --------
        >>> data=Tableau(['date','Temperature','jour_de_la_semaine','nbr_jour_pluie'],[['18/01/2022',20,'mardi',3],['19/01/2022',22,'mercredi',6],['20/02/2022',25,'jeudi',11]])
        >>> a1=Exportation("csv","C:/Users/Cédric/OneDrive/Documents/ENSAI/ptd/donnees/","fichierla")
        >>> a1.transformation(data)
        """
        
        if self.type_sortie=="csv":
            list = []
            list.append(donnees.header)
            for row in donnees.body:
                list.append(row)
            with open(self.dossier+self.nom_fichier_a_ecrire+'.csv', 'w', newline='') as file:
                writer = csv.writer(file, quoting=csv.QUOTE_ALL,delimiter=';')
                writer.writerows(list)
        
        elif self.type_sortie=="carte":
            return("lancer le module cartoplot")
        
        else:
            raise ValueError("Type de sortie non pris en compte")



if __name__ == "__main__":
    import doctest
    doctest.testmod()